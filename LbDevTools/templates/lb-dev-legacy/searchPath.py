from LbEnv.ProjectEnv.options import (
    EnvSearchPathEntry,
    LHCbDevPathEntry,
    NightlyPathEntry,
    SearchPath,
    SearchPathEntry,
)

path = ${search_path_repr}
